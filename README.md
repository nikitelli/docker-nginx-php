# docker-nginx-php

long term goal: 
- building a Centos7 docker image with nginx and php included. When pointing to port 8080, the php version should be shown

- 1st example shows the solution with docker-compose
- 2nd example shows the solution by building the image

# pre-requisistes:

put all files into the same directory (don't forget the www subdirecory), and run the docker command.

# 1) running the containers with docker-compose:

```
$ docker-compose -f docker-compose.yaml up
```

Output is like the following:

```
Creating vagrant_php_1 ... 
Creating vagrant_php_1 ... done
Creating vagrant_web_1 ... 
Creating vagrant_web_1 ... done
Attaching to vagrant_php_1, vagrant_web_1
php_1  | [16-May-2021 18:51:36] NOTICE: fpm is running, pid 1
php_1  | [16-May-2021 18:51:36] NOTICE: ready to handle connections
web_1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
web_1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
web_1  | 10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
web_1  | 10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
```

# access the app (ip address must be adjusted to your needs):

hint: the ip address is the node address where the docker cmd is run

```
http://{your_ip}:8080/
```

# 2) building a Centos7 image with nginx and php, and start the container:

download the Dockerfile including the www directory<p>
<b>important<b>: adjust the ip address in site.conf according to your node, where the docker cmd is run

build and start the container:

```
$ docker build -t nginx-fk .
$ docker run -d -p 8080:80 --privileged nginx-fk
```

# access the app (ip address must be adjusted to your needs):

hint: the ip address is the node address where the docker cmd is run

```
http://{your_ip}:8080/
```

