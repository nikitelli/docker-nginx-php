# Dockerfile
FROM centos:7
LABEL maintainer="fritz.n.kraus@gmail.com"
LABEL version="0.9"
LABEL description="docker image based on Centos7 with nginx and php"
USER root
ENV container docker
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/sys/fs/cgroup" ]
RUN yum install -y epel-release
RUN yum update -y

# nginx
RUN yum install -y nginx

# php
RUN yum install -y yum-utils
RUN yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
RUN yum-config-manager --enable remi-php73
RUN yum install -y php php-fpm php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd

COPY site.conf /etc/nginx/conf.d/site.conf
WORKDIR www
COPY www/index.php .
COPY www/index.html .

EXPOSE 80

RUN systemctl enable nginx
RUN systemctl enable php-fpm

CMD ["/usr/sbin/init"]